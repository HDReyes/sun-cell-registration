<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_signup extends CI_Model {

		function signUp()
		{
				$return['status']  = "";
				$return['message'] = "";

				$name   = $this->input->post('name');
				$email  = $this->input->post('email');
				$mobile = $this->input->post('mobile');

				//Check if Email is already registered
				if($this->_checkEmailEntry($email))
				{
					//If TRUE: Email is already registered notify user that account is registered
					$return['status']  = false;
					$return['message'] = "This E-mail address has already been registered";

				}else{

					//IF FALSE: Continue the user registration

						/*** BEGIN: Sets all value to be inserted on tbl_registration ***/
						$values = 'name 			=	"' . mysql_real_escape_string($name) . '", '.
								  'email 			=	"' . mysql_real_escape_string($email) . '", '.
								  'mobile 			=	"' . mysql_real_escape_string($mobile) . '", '.
								  'date_registered	=	"' . date('Y-m-d H:i:s') . '"';
						/*** END: Sets all value to be inserted on tbl_registration ***/

						$query = 'INSERT INTO tbl_registration SET ' . $values;
						$insert = $this->db->query($query);
						if($this->db->affected_rows() > 0)
						{
							if($this->_notify_via_email($email))
							{
								$return['status']  = true;
								$return['message'] = "You have successfully registered to 'NAME OF PROMO HERE', please check your E-mail for the confirmation. (You may want to look at the spam, just to be sure) ";

							}
						}else{
							$return['status']  = false;
							$return['message'] = "Oops, something went wrong. Try re-registering, Thank you!";
						}
				}

				return $return;
		}

		//Returns TRUE or FALSE
		//TRUE - the email account is already registered
		//FALSE - the email address is non-existent
		private function _checkEmailEntry($email = "")
		{
			$return = false;
			
			$sql = "SELECT email FROM tbl_registration WHERE email = ?";
			$query = $this->db->query($sql, array($email)); 
			if($query->num_rows() > 0)
			{
				$return = true;
			}

			return $return;
		}

		private function _notify_via_email($email = "")
		{
			/**
			 * newsletter_signup_email function send an email notification to administrator
			 * $email - users email passed
			 */
			 $message ="<table width=\"500px\" border=\"0\" cellspacing=\"0\" cellpadding=\"8\" style=\"font-family:arial; font-size:13px; border:solid 1px #cccccc\">
							<tr>
								<td style=\"background:#cccccc; color:#000000; font-weight:bold\">Email:</td>
								<td>".$email."</td>
							</tr>
						</table>";				
			
			$this->load->library('email');
	
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';
			$config['smtp_host'] = 'smtp.gmail.com';
			$config['smtp_port'] = 465;
			//$config['smtp_user'] = 'email';
			//$config['smtp_pass'] = 'pass';
			$this->email->initialize($config);
	
			$this->email->set_newline("\r\n");
			$this->email->from('ironman@havocdigital.com', 'Juan Dela Cruz');
			$this->email->to('hajji@havocdigital.com');		
			//$this->email->bcc('hajji@havocdigital.com');		

			$this->email->subject('Thank you for signing-up!');		
			$this->email->message($message);
			//return TRUE;
			if($this->email->send($message))
			{
				return TRUE;
			}else
			{
				return FALSE;
				//show_error($this->email->print_debugger());
			}				
		}

}
