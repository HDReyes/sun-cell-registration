<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>Sun Cellular Registration</title>
    <script src="<?php echo PROD_JS_LIB; ?>jquery.js"></script>
    <script src="<?php echo PROD_JS_LIB; ?>jquery.mockjax.js"></script>
    <script src="<?php echo PROD_JS_LIB; ?>jquery.validate.js"></script>
    <script src="<?php echo PROD_JS_LIB; ?>jquery.mask_1.3.js"></script>

    <style>
        
        input[type=text]{
                width:250px;
                border-radius: 3px;
                padding: 8px 8px 8px 8px;
                border: solid 1px #cccccc;
        }
        
        label{
            margin:20px 20px 25px 3px;
            font-family: arial;
            font-weight: normal;
        }

        input[type=submit] {
            padding:10px 20px; 
            background:#f00; 
            border:solid 1px #f00;
            cursor:pointer;
            -webkit-border-radius: 5px;
            border-radius: 5px; 
            color: #ffffff;
        }

        .formRow
        {
            border:solid 1px red;
            margin: 0px 0px 15px 0px;
        }

/*        #signupform label.error {
          background:url("<?php echo PROD_IMAGES; ?>unchecked.gif") no-repeat 0px 0px;
          padding-left: 16px;
          padding-bottom: 2px;
          font-weight: bold;
          color: #EA5200;
        }

        #signupform label.checked {
          background:url("<?php echo PROD_IMAGES; ?>checked.gif") no-repeat 0px 0px;
        }
*/        
.error {
    border: 1px solid #f00 !important;
}

.valid {
    border: 1px solid #cccccc;
}
    </style>

    <script>
    $(document).ready(function() {
        $("#mobile").mask("9999-9999999");
        // $.mockjax({
        //     url: "emails.action",
        //     response: function(settings) {
        //         var email = settings.data.email,
        //             emails = ["glen@marketo.com", "george@bush.gov", "me@god.com", "aboutface@cooper.com", "steam@valve.com", "bill@gates.com"];
        //         this.responseText = "true";
        //         if ($.inArray(email, emails) !== -1) {
        //             this.responseText = "false";
        //         }
        //     },
        //     responseTime: 500
        // });

        // $.mockjax({
        //     url: "users.action",
        //     response: function(settings) {
        //         var user = settings.data.username,
        //             users = ["asdf", "Peter", "Peter2", "George"];
        //         this.responseText = "true";
        //         if ($.inArray(user, users) !== -1) {
        //             this.responseText = "false";
        //         }
        //     },
        //     responseTime: 500
        // });

        // validate signup form on keyup and submit
        var validator = $("#signupform").validate({
            rules: {
                name: "required",
                mobile: {
                    required: true,
                    minlength: 11
                },
                email: {
                    required: true,
                    email: true,
                },
            },
            messages: {
                name: "Enter your name",
                mobile: {
                    required: "Enter a mobile number",
                    minlength: jQuery.validator.format("Enter at least {0} characters")
                },
                email: {
                    required: "Please enter a valid email address",
                    minlength: "Please enter a valid email address"
                }
            },
            // the errorPlacement has to take the table layout into account
            errorPlacement: function(error, element) {
                    //error.appendTo(element.parent().next());
                    //console.log(error);
                    return false;
            },
            // specifying a submitHandler prevents the default submit, good for the demo
            submitHandler: function() {
                //alert("submitted!");
                form.submit();
            }
            // ,
            // // set this class to error-labels to indicate valid fields
            // success: function(label) {
            //     // set &nbsp; as text for IE
            //     label.html("&nbsp;").addClass("checked");
            // },
            // highlight: function(element, errorClass) {
            //     $(element).parent().next().find("." + errorClass).removeClass("checked");
            // }
        });

    });

    </script>

</head>
<body>



