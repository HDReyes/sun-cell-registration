<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//parent::__construct();
		$data['page'] = 'registration-form';
		$this->load->view('template/template', $data);
	}

	//validate
	public function validate()
	{

		$this->form_validation->set_message('required','Oops, make sure to fill-out the necessary fields.');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|email|xss_clean');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|xss_clean');

		//IF Validation returns FALSE reload index and display error message
		//ELSE load sign-up model and call signUp function
		if ($this->form_validation->run() == FALSE)
		{
			$this->index();
		}else{
			
			$this->load->model('mod_signup');
			$response = $this->mod_signup->signUp();
			//print_r($response);
			if ($response['status']) {

				# Redirect to success page
				$this->session->set_flashdata('status_err_msg', $response['message']);
				redirect(base_url().'registration/success/');

			}else{

				# Redirect to index page with post error message
				$this->session->set_flashdata('status_err_msg', $response['message']);
				redirect(base_url());
			}
		}		

	}

	public function success()
	{
		//echo $this->session->flashdata('status_err_msg'); 
		$data['page'] = 'success-page';
		$this->load->view('template/template', $data);

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */